import React from 'react';
import logo from './logo.svg';
import './App.css';

class AppContainer extends React.Component{

    state = {};

    render() {
        return (
            <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                This is a regular P tag..
              </p>
              <a
                className="App-link"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
              >
                Learn React
              </a>
            </header>
          </div>
        )
    }

}

export default AppContainer;
