import React, {useState } from "react";
import { loginUser } from "../../api/user.api";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState("");
  const [loginSuccess, setLoginSuccess] = useState("");

  const onLoginClicked = async (ev) => {
    setIsLoading(true);

    try {
        const result = await loginUser(username, password);
        console.log(result);
        setLoginSuccess(`Successfully logged in user: ${username}`)
    } catch (e) {
        setLoginError(e.message || e);
    } finally {
        setIsLoading(false);
    }
  };

  const onUsernameChanged = (ev) => setUsername(ev.target.value.trim());
  const onPasswordChanged = (ev) => setPassword(ev.target.value.trim());

  return (
    <form>
      <div>
        <label>Username: </label>
        <input 
          type="text" 
          name="username" 
          placeholder="Enter your username"
          onChange={onUsernameChanged} 
          />
      </div>

      <div>
        <label>Password: </label>
        <input
          type="password"
          name="password"
          placeholder="Enter your password"
          onChange={onPasswordChanged}
        />
      </div>

      <div>
        <button type="button" onClick={onLoginClicked}>Login</button>
      </div>

      { isLoading && <div>Logging in...</div> }
      
      {loginError && <div>{loginError}</div> }

      { loginSuccess && <div>{loginSuccess}</div> }

    </form>
  );
};

export default LoginForm;
