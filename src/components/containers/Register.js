import React from 'react';
import RegisterForm from '../forms/RegisterForm';

const Register = () => {

    const handleRegisterClicked = (result) => {
        console.log('Trigged from RegisterForm', result);
        
    }

    return (

        <div>
            <h1>Register to Survey Puppy</h1>
            <RegisterForm click={ handleRegisterClicked } />
        </div>
    );

};

export default Register;