import React from 'react';
import LoginForm from '../forms/LoginForm'

const Login = () => {

    const handleLoginClicked = (result) => {
        console.log('Trigged from loginform', result);
        
    }

    return (
        <div>
            <h1>Login to Survey Puppy</h1>
            <LoginForm click={ handleLoginClicked } />
        </div>
    )
};

export default Login;