const API_REGISTER_URL =  "https://survey-poodle.herokuapp.com/v1/api/users/register";
const API_LOGIN_URL = "https://survey-poodle.herokuapp.com/v1/api/users/login";

const CreateFetchOptions = (method, body) => ({
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });


export const registerUser = (username, password) =>{
    return fetch(API_REGISTER_URL,
        CreateFetchOptions('POST', {
            user: {
                username,
                password
            }
        }))
        .then(r => r.json())
        .then(resp => {
            if (resp.status >=400) {
                throw Error(resp.error);
            }
            return resp;
        })
}


export const loginUser = (username, password) =>{
    return fetch(API_LOGIN_URL,
        CreateFetchOptions('POST', {
            user: {
                username,
                password
            }
        }))
    .then(r => r.json())
    .then(resp => {
        if (resp.status >=400) {
            throw Error(resp.error);
        }
        return resp;
    })
}